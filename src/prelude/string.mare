:class val String
  :prop _size USize
  :prop _alloc USize // TODO: rename as `_space`
  :prop _ptr CPointer(U8)'ref
  :new from_cpointer (@_ptr, @_size, @_alloc)
  :new val val_from_cpointer (@_ptr, @_size, @_alloc) // TODO: remove this and use recover instead?
  :fun cpointer CPointer(U8): @_ptr
  :fun hash: @_ptr._hash(@_size)
  :fun size: @_size
  
  :new (space USize = 0)
    @_size = 0
    @_alloc = space.max(8)
    @_ptr = CPointer(U8)._alloc(@_alloc)
  
  :fun "==" (other String'box)
    (@_size == other._size) && (@_ptr._compare(other._ptr, @_size) == 0)
  
  :: Returns true if the string is null-terminated and safe to pass to an FFI
  :: function that doesn't accept a size argument, expecting a null-terminator.
  ::
  :: This method checks that there is a null byte just after the final position
  :: of populated bytes in the string, but does not check for other null bytes
  :: which may be present earlier in the content of the string.
  ::
  :: Use the cstring method to get a null-terminated version of the string.
  
  :fun is_null_terminated: (@_alloc > @_size) && (@_ptr._get_at(@_size) == 0)
  
  :: Returns a CPointer to a null-terminated version of this string,
  :: copying the string if necessary to get a null-terminated pointer.
  ::
  :: Call the cpointer method instead when you don't need a null terminator
  :: (that is, when the FFI function you are calling has a size argument).
  
  :fun cstring
    if @is_null_terminated (
      @_ptr
    |
      ptr = @_ptr._alloc(@_size + 1)
      @_ptr._copy_to(ptr._unsafe, @_size)
      ptr._assign_at(@_size, 0)
      ptr
    )
  
  :: Ensure enough capacity is allocated for the given space, in bytes.
  :: An additional byte will be reserved for a null terminator.
  
  :fun ref reserve (space)
    if (@_alloc <= space) (
      max_space = USize[-1] // TODO: USize.max_value ?
      if (space < (max_space / 2)) (
        @_alloc = space.next_pow2
      |
        @_alloc = space
      )
      @_ptr = @_ptr._realloc(@_alloc)
    )
  
  :fun ref push_byte (byte U8)
    @reserve(@_size + 1)
    @_ptr._assign_at(@_size, byte)
    @_size = @_size + 1 // TODO: use += operator
    @
  
  :fun each_byte
    index USize = 0
    while (index < @_size) (
      yield @_ptr._get_at(index)
      index = index + 1
    )
    @
  
  :fun each_byte_with_index
    index USize = 0
    while (index < @_size) (
      yield (@_ptr._get_at(index), index)
      index = index + 1
    )
    @
  
  :fun ref "<<" (other String'box)
    new_size = @_size + other._size
    @reserve(new_size)
    if other.is_null_terminated (
      other._ptr._copy_to(@_ptr._offset(@_size), other.size + 1)
    |
      other._ptr._copy_to(@_ptr._offset(@_size), other.size)
    )
    @_size = new_size
    @
  
  :fun val split (split_byte U8) // TODO: a byte shouldn't be the only thing we can split by...
    result Array(String) = []
    @each_split(split_byte) -> (s | result << s)
    result
  
  :fun val each_split (split_byte U8) // TODO: a byte shouldn't be the only thing we can split by...
    :yields String
    finish USize = 0
    start = finish
    while (finish < @_size) (
      byte = @_ptr._get_at(finish)
      if (byte == split_byte) (
        yield @val_from_cpointer(
          @_ptr._offset(start)
          (finish - start)
          @_alloc - start
        )
        finish = finish + 1
        start = finish
      |
        finish = finish + 1
      )
    )
    yield @val_from_cpointer(
      @_ptr._offset(start)
      (finish - start)
      @_alloc - start
    )
    @
  
  :fun parse_i64! // TODO: Use something like Crystal's Char::Reader instead?
    output I64 = 0
    possible_negation I64 = 1
    @each_byte_with_index -> (byte, index |
      case (
      | byte == '+' && index == 0 | // do nothing
      | byte == '-' && index == 0 | possible_negation = -1
      | byte >= '0' && byte <= '9' | output = output * 10 + (byte - '0').i64
      | error!
      )
    )
    output * possible_negation
